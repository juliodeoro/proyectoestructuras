#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

using namespace std;

struct persona;

typedef struct tiquete {
    int tipo;
    const char *codigo;
    char clase[20];
    //char fecha_salida[11];
    int dia_salida;
    int mes_salida;
    int anio_salida;
    //char hora_salida[9];
    int hh_salida;
    int mm_salida;
    //char fecha_llegada[11];
    int dia_llegada;
    int mes_llegada;
    int anio_llegada;
    //char hora_llegada[9];
    int hh_llegada;
    int mm_llegada;
    int silla;
    persona *asigancion;
    struct tiquete *sig;
    struct tiquete *ant;
}vuelo;

typedef struct persona{
    char documento[30];
    char nombre[255];
    char apellido[255];
    char telefono[20];
    //char fecha_nacimiento[11];
    int dia_nacimiento;
    int mes_nacimiento;
    int anio_nacimiento;
    char genero;
    vuelo *tiket;
    struct persona *sig;
    struct persona *ant;
}pasajero;

typedef pasajero *tpasajero;
typedef vuelo *tvuelo;

void clearScreen (){
    system("clear");
    //system("cls");
}

void menu() {
    cout << "//////////////////////  GOLONDRINA VELOZ  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" << endl;
    cout << "//////////////////////////  TIQUETES  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" << endl;
    cout << "  1. Comprar Tiquete            |  5. Cambiar Silla             " << endl;
    cout << "  2. Modificar Pasajero         |  6. Imprimir pase de abordar  " << endl;
    cout << "  3. Listar Pasajeros           |  7. Cancelar Tiquete          " << endl;
    cout << "  4. Buscar pasajero            |  8. Salir                     " << endl;
    cout << "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ ///////////////////////////////" << endl;
}

int validarEdad(int dia, int mes, int anio){

    time_t tiempo;
    struct tm *fecha_local;
    tiempo = time(NULL);
    fecha_local = localtime(&tiempo);
   
    int dia_actual = fecha_local->tm_mday;
    int mes_actual = fecha_local->tm_mon+1;
    int anio_actual = fecha_local->tm_year+1900;

    if (dia > 0 && dia <= 31){
        if (mes > 0 && mes <= 12) {
            if (anio > 1900 && anio <= anio_actual){
                int dias_mes[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                int anios;
                if (mes == 2){
                    if (dia <= 29){
                        if ((anio_actual % 4 == 0 && anio_actual % 100 != 0) || (anio_actual % 400 == 0)){
                            dias_mes[mes-1]=29;
                        }
                    }else{
                        cout << "Dia no valido."; 
                        return 0; // Fecha no valida
                    }
                }else if (mes == 4 || mes == 6 || mes == 9 || mes == 11){
                    if (dia > 30){
                        cout << "Dia no valido."; 
                        return 0; // Fecha no valida
                    }
                }
                if (dia > dia_actual){
                    dia_actual += dias_mes[mes-2]; // + dias del mes anterior al actual
                    mes_actual -= 1;
                }
                if (mes > mes_actual){
                    mes_actual += 12;
                    anio_actual -= 1;
                }
                // dias = dia_actual - dia;
                // meses = mes_actual - mes;
                anios = anio_actual - anio;
                return anios;
            
            }else{
                cout << "Anio no valido.";
                return 0;
            }
        }else{
            cout << "Mes no valido.";
            return 0;
        }
    }else{
        cout << "Dia no valido."; 
        return 0;        
    }
    return 0; //Fecha no valida
}

int validarFecha(int dia, int mes, int anio){

    time_t tiempo;
    struct tm *fecha_local;
    tiempo = time(NULL);
    fecha_local = localtime(&tiempo);
   
    int dia_actual = fecha_local->tm_mday;
    int mes_actual = fecha_local->tm_mon+1;
    int anio_actual = fecha_local->tm_year+1900;

    if (anio >= anio_actual && anio <= anio_actual+2){
        if (mes > 0 && mes <= 12) {
            if (dia > 0 && dia <= 31){
                int dias_mes[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                int anios;
                if (mes == 2){
                    if (dia <= 29){
                        if ((anio_actual % 4 == 0 && anio_actual % 100 != 0) || (anio_actual % 400 == 0)){
                            dias_mes[mes-1]=29;
                        }
                    }else{
                        cout << "Dia no valido."; 
                        return 0; // Fecha no valida
                    }
                }else if (mes == 4 || mes == 6 || mes == 9 || mes == 11){
                    if (dia > 30){
                        cout << "Dia no valido."; 
                        return 0; // Fecha no valida
                    }
                }
                if (anio == anio_actual){
                    if(mes >= mes_actual){
                        if (dia >= dia_actual){
                            return 1;
                        }else{
                            cout << "Dia caducado.";
                            return 0;
                        }
                    }else{
                        cout << "Mes caducado.";
                        return 0;
                    }
                }
                return 1;
            }else{
                cout << "Dia no valido.";
                return 0;
            }
        }else{
            cout << "Mes no valido.";
            return 0;
        }
    }else{
        cout << "Anio no valido."; 
        return 0;        
    }
    return 0; //Fecha no valida
}

int validarHora(int horas, int minutos){
    if (horas >= 0 && horas < 24){
        if (minutos >= 0 && minutos < 60){
            return 1;
        }else{
            cout << "Munito no valido.";
            return 0;
        }
    }else{
        cout << "Hora no valida.";
        return 0;
    }
}

void buscarPasajero(tpasajero cabezap) {
    char doc[30];
    bool flag;

    cout << "///  Buscar Pasajero  ///\n" << endl;
    
    if (cabezap != NULL){
        cout << "Ingrese documento del pasajero: ";
        cin >> doc;
        cout << endl; 
        while (cabezap != NULL)
        {
            if (strcmp(cabezap->documento, doc) == 0){
                cout << "Nombre: " << cabezap->nombre << endl;
                cout << "Apellido: " << cabezap->apellido << endl;
                cout << "Telefono: " << cabezap->telefono << endl;
                //cout << "Fecha nacimiento: " << cabezap->fecha_nacimiento << endl;
                cout << "Fecha nacimiento: ";
                cout << cabezap->dia_nacimiento << "/";
                cout << cabezap->mes_nacimiento << "/";
                cout << cabezap->anio_nacimiento << endl;
                cout << "Genero: " << cabezap->genero << endl;
                cout << "Vuelo: " << cabezap->tiket->codigo << endl;
                cout << "Clase: " << cabezap->tiket->clase << endl; 
                cout << "Fecha Salida (";
                cout << cabezap->tiket->dia_salida << "/";
                cout << cabezap->tiket->mes_salida << "/";
                cout << cabezap->tiket->anio_salida << ") ";
                cout << "a las (";
                cout << cabezap->tiket->hh_salida << ":";
                cout << cabezap->tiket->mm_salida << ")." << endl;
                cout << "Fecha llegada (";
                cout << cabezap->tiket->dia_llegada << "/";
                cout << cabezap->tiket->mes_llegada << "/";
                cout << cabezap->tiket->anio_llegada << ") ";
                cout << "a las (";
                cout << cabezap->tiket->hh_llegada << ":";
                cout << cabezap->tiket->mm_llegada << ")." << endl;
                cout << "Silla: " << cabezap->tiket->silla << endl << endl;
                flag = true;
                break;
            }
            cabezap = cabezap->sig;
        }
        if (!flag) cout << "No existe.\n" << endl; 
    }else{
        cout << "No hay pasajeros para listar.\n" << endl;
    }
}

void modificarPasajero (tpasajero* cabezap) {
    cout << "///  Modificar Pasajero  ///\n" << endl;
    char doc[30];
    int dia, mes, anio;
    bool flag;
    
    if ((*cabezap) != NULL){
        cout << "Ingrese documento del pasajero: ";
        cin >> doc;
        cout << endl; 
        while ((*cabezap) != NULL)
        {
            if (strcmp((*cabezap)->documento, doc) == 0){
                // Imprime datos actuales
                cout << "Nombre: " << (*cabezap)->nombre << endl;
                cout << "Apellido: " << (*cabezap)->apellido << endl;
                cout << "Telefono: " << (*cabezap)->telefono << endl;
                cout << "Fecha nacimiento: ";
                cout << (*cabezap)->dia_nacimiento << "/";
                cout << (*cabezap)->mes_nacimiento << "/";
                cout << (*cabezap)->anio_nacimiento << endl;
                cout << "Genero: " << (*cabezap)->genero << endl << endl;
                // Nuevos datos
                cout << "Ingrese los nuevos datos.\n" << endl;
                cout << "Nombre: ";
                cin >> (*cabezap)->nombre;
                cout << "Apellido: ";
                cin >> (*cabezap)->apellido;
                cout << "Telefono: ";
                cin >> (*cabezap)->telefono;
                cout << "Fecha de nacimiento (DD/MM/AAAA): ";
                scanf ("%d/%d/%d", &dia, &mes, &anio);
                while (validarEdad(dia,mes,anio) < 18)
                {
                    cout << " Intente de nuevo (+18): ";
                    scanf ("%d/%d/%d", &dia, &mes, &anio);
                }
                (*cabezap)->dia_nacimiento = dia;
                (*cabezap)->mes_nacimiento = mes;
                (*cabezap)->anio_nacimiento = anio;
                cout << "Genero: " << endl << "  M: Masculino \n  F: Femenino \n  O: Otro" << endl << "Cual? (M/F/O): ";
                cin >> (*cabezap)->genero;
                
                flag = true;
                break;
            }
            *cabezap = (*cabezap)->sig;
        }
        if (!flag) cout << "No existe.\n" << endl; 
    }else{
        cout << "No hay pasajeros para listar.\n" << endl;
    }
    
}

void listarPasajeros (tpasajero cabezap) {
    cout << "///  Listar Pasajeros  ///\n" << endl;
    if (cabezap != NULL){
        cout << "Documento | Nombre | Apellido | Telefono | F. Nacimiento | Genero | Silla " << endl;
        while (cabezap != NULL)
        {
            cout << cabezap->documento << " | ";
            cout << cabezap->nombre << " | ";
            cout << cabezap->apellido << " | ";
            cout << cabezap->telefono << " | ";
            //cout << cabezap->fecha_nacimiento << " | ";
            cout << cabezap->dia_nacimiento << "/";
            cout << cabezap->mes_nacimiento << "/";
            cout << cabezap->anio_nacimiento << " | ";
            cout << cabezap->genero << " | ";
            cout << cabezap->tiket->silla << endl;
        cabezap = cabezap->sig;
        }
        cout << endl;
    }else{
        cout << "No hay pasajeros para listar.\n" << endl;
    }
}

int buscarSilla (tvuelo ctiquete, int silla) {
    while (ctiquete != NULL)
    {
        if(ctiquete->silla == silla){
            return 1;
        }
        ctiquete = ctiquete->sig;
    }
    return 0;
}

void imprimirPaseAbordar (tpasajero cabezap) {
    cout << "///  Imprimir Pase de Abordar  ///\n" << endl; 
    char doc[30];
    bool flag;
    
    if (cabezap != NULL){
        cout << "Ingrese documento del pasajero: ";
        cin >> doc;
        cout << endl; 
        while (cabezap != NULL)
        {
            if (strcmp(cabezap->documento, doc) == 0){
                cout << cabezap->tiket->codigo;
                cout << " Vuelo ";
                if (cabezap->tiket->tipo == 1){
                    cout << "Nacional." << endl;
                }else{
                    cout << "Internacional." << endl;
                };           
                cout << "Pasajero: ";
                cout << cabezap->nombre << " ";
                cout << cabezap->apellido << endl;
                cout << "Documento: " << cabezap->documento << endl; 
                cout << "Telefono: " << cabezap->telefono << endl;
                //cout << "Fecha nacimiento: " << cabezap->fecha_nacimiento << endl;
                cout << "Fecha nacimiento: ";
                cout << cabezap->dia_nacimiento << "/";
                cout << cabezap->mes_nacimiento << "/";
                cout << cabezap->anio_nacimiento << endl;
                cout << "Genero: " << cabezap->genero << endl;
                cout << "Clase: " << cabezap->tiket->clase << endl; 
                cout << "Fecha Vuelo (";
                cout << cabezap->tiket->dia_salida << "/";
                cout << cabezap->tiket->mes_salida << "/";
                cout << cabezap->tiket->anio_salida << ") ";
                cout << "a las (";
                cout << cabezap->tiket->hh_salida << ":";
                cout << cabezap->tiket->mm_salida << ")." << endl;
                cout << "Fecha llegada (";
                cout << cabezap->tiket->dia_llegada << "/";
                cout << cabezap->tiket->mes_llegada << "/";
                cout << cabezap->tiket->anio_llegada << ") ";
                cout << "a las (";
                cout << cabezap->tiket->hh_llegada << ":";
                cout << cabezap->tiket->mm_llegada << ")." << endl;
                cout << "Silla: " << cabezap->tiket->silla << endl << endl;
                flag = true;
                break;
            }
            cabezap = cabezap->sig;
        }
        if (!flag) cout << "No existe.\n" << endl; 
    }else{
        cout << "No hay pasajeros para listar.\n" << endl;
    }
}

void cancelarTiquete (tvuelo* cvuelo) {
    int sillaBorrar;

    cout << "Ingrese en numero de la silla para eliminar el tiquete";
    cin >> sillaBorrar;

    while (*cvuelo != NULL)
    {
        if((*cvuelo)->silla == sillaBorrar){
            if((*cvuelo)->ant != NULL){
                (*cvuelo)->ant->sig = (*cvuelo)->sig;
            }

            if((*cvuelo)->sig != NULL){
                (*cvuelo)->sig->ant = (*cvuelo)->ant;
            }
            
            free(*cvuelo);
        }else{
            *cvuelo = (*cvuelo)->sig;
        }
    }
}

int asignarSilla(tvuelo* cvuelo, int claseVuelo){
    int numsilla;

    while (buscarSilla(*cvuelo, numsilla) == 1)
    {
        if(claseVuelo == 1){
            numsilla = rand() % 21;
        }else{
            numsilla = (rand() % 251) + 20;
        }
    }

    return numsilla;
}

int comprarTiquete (tvuelo* cvuelo, tpasajero* cpasajero) {
    tpasajero nuevo_pasajero = (pasajero*)malloc(sizeof(persona));
    tvuelo nuevo_tiquete = (vuelo*)malloc(sizeof(tiquete));
    int dia, mes, anio, hh, mm, tipoclase;

    cout << "///  Comprar Tiquete  ///\n" << endl;
    
    cout << "Que tipo de vuelo es? " << endl;
    cout << "  1. Nacional" << endl;
    cout << "  2. Internacional" << endl;
    cout << "Tipo de Vuelo: ";
    cin >> nuevo_tiquete->tipo;
    while (nuevo_tiquete->tipo < 1 || nuevo_tiquete->tipo > 2)
    {
        cout << "Incorrecto, intente de nuevo: ";
        cin >> nuevo_tiquete->tipo;
    }
    if(nuevo_tiquete->tipo == 1) {
        nuevo_tiquete->codigo = "GOPLA01"; 
    }else{ 
        nuevo_tiquete->codigo = "GOPLA02";
    }
    cout << "Codigo de Vuelo: " << nuevo_tiquete->codigo << endl;
    cout << "Documento pasajero: ";
    cin >> nuevo_pasajero->documento;
    cout << "Nombre pasajero: ";
    cin >> nuevo_pasajero->nombre;
    cout << "Apellido pasajero: ";
    cin >> nuevo_pasajero->apellido;
    cout << "Teléfono pasajero: ";
    cin >> nuevo_pasajero->telefono;
    cout << "Fecha de nacimiento (DD/MM/AAAA): ";
    //cin >> nuevo_pasajero->fecha_nacimiento;
    scanf ("%d/%d/%d", &dia, &mes, &anio);
    while (validarEdad(dia,mes,anio) < 18)
    {
        cout << " Intente de nuevo (+18): ";
        scanf ("%d/%d/%d", &dia, &mes, &anio);
    }
    nuevo_pasajero->dia_nacimiento = dia;
    nuevo_pasajero->mes_nacimiento = mes;
    nuevo_pasajero->anio_nacimiento = anio;
    cout << "Genero: " << endl << "  M: Masculino \n  F: Femenino \n  O: Otro" << endl << "Cual? (M/F/O): ";
    cin >> nuevo_pasajero->genero;
    cout << "Clase de tiquete (1. primera ó 2. economica): ";
    cin >> tipoclase;
    memcpy(nuevo_tiquete->clase,  (tipoclase == 1 ? "primera" : "economica"), sizeof nuevo_tiquete->clase);
    cout << "Fecha vuelo (DD/MM/AAAA): ";
    //cin >> nuevo_tiquete->fecha_salida;
    scanf ("%d/%d/%d", &dia, &mes, &anio);
    while (validarFecha(dia,mes,anio) != 1)
    {
        cout << " Intente de nuevo: ";
        scanf ("%d/%d/%d", &dia, &mes, &anio);
    }
    nuevo_tiquete->dia_salida = dia;
    nuevo_tiquete->mes_salida = mes;
    nuevo_tiquete->anio_salida = anio;
    cout << "Hora salida (HH:MM): ";
    //cin >> nuevo_tiquete->hora_salida;
    scanf ("%d:%d", &hh, &mm);
    while (validarHora(hh,mm) == 0){
        cout << " Intente de nuevo: ";
        scanf ("%d:%d", &hh, &mm);
    }
    nuevo_tiquete->hh_salida = hh;
    nuevo_tiquete->mm_salida = mm;
    if (nuevo_tiquete->tipo == 1){
        // Nacional +50 min
        if (mm > 10){
            if (hh > 22){
                dia = dia+1;
                hh = hh+1-24;
            }else{
                hh = hh+1;                
            }
            mm = mm+50-60;
        }else{
            mm = mm+50;
        }
    }else{
        // Internacional +11 horas
        if (hh > 13){
            dia = dia+1;
            hh = hh+11-24;
        }else{
            hh = hh+11;
        }
    }
    nuevo_tiquete->hh_llegada = hh;
    nuevo_tiquete->mm_llegada = mm;
    nuevo_tiquete->dia_llegada = dia;
    nuevo_tiquete->mes_llegada = mes;
    nuevo_tiquete->anio_llegada = anio;
    cout << "Fecha llegada (";
    cout << nuevo_tiquete->dia_llegada << "/";
    cout << nuevo_tiquete->mes_llegada << "/";
    cout << nuevo_tiquete->anio_llegada << ") ";
    cout << "a las (";
    cout << nuevo_tiquete->hh_llegada << ":";
    cout << nuevo_tiquete->mm_llegada << ")." << endl;
    nuevo_tiquete->silla = asignarSilla(cvuelo, tipoclase);
    cout << endl;
    
    nuevo_pasajero->tiket = nuevo_tiquete;
    nuevo_tiquete->asigancion = nuevo_pasajero;

    if(*cpasajero == NULL){
        //cout << "Nuevo pasajero vacio";
        *cpasajero = nuevo_pasajero;
        (*cpasajero)->sig = NULL;
        (*cpasajero)->ant = NULL;
    }else{
        //cout << "Nuevo pasajero a la derecha";
        nuevo_pasajero->sig = (*cpasajero)->sig;
        (*cpasajero)->sig = nuevo_pasajero;
        nuevo_pasajero->ant = *cpasajero;

        if(nuevo_pasajero->sig != NULL){
            nuevo_pasajero->sig->ant = nuevo_pasajero;
        }
    }  

    if(*cvuelo == NULL){
        //cout << "Nuevo tickete vacio";
        *cvuelo = nuevo_tiquete;
        (*cvuelo)->sig = NULL;
        (*cvuelo)->ant = NULL;
        cout << "Gracias por su compra!." << endl;
        return 1;
    }else{
        while (*cvuelo != NULL)
        {
            if((*cvuelo)->silla > nuevo_tiquete->silla){
                //cout << "Nuevo tickete al inicio";
                nuevo_tiquete->sig = *cvuelo;
                nuevo_tiquete->ant = (*cvuelo)->ant;
                (*cvuelo)->ant = nuevo_tiquete;
                cout << "Gracias por su compra!.\n" << endl;
                return 1;
            }else if((*cvuelo)->sig == NULL){
                //cout << "Nuevo tickete al final";
                nuevo_tiquete->sig = (*cvuelo)->sig;
                (*cvuelo)->sig = nuevo_tiquete;
                nuevo_tiquete->ant = *cvuelo;
                cout << "Gracias por su compra!.\n" << endl;
                return 1;
            }else if ((*cvuelo)->sig->silla > nuevo_tiquete->silla){
                //cout << "Nuevo tickete en medio";
                nuevo_tiquete->sig = (*cvuelo)->sig;
                (*cvuelo)->sig = nuevo_tiquete;
                nuevo_tiquete->ant = *cvuelo;
                nuevo_tiquete->sig->ant = nuevo_tiquete;
                cout << "Gracias por su compra!.\n" << endl;
                return 1;
            }else{
                *cvuelo = (*cvuelo)->sig;
            }
        }  
    }

    cout << "Gracias por su compra!.\n" << endl;
    return 0;
}

int cambiarSilla (tvuelo* cvuelo, tvuelo listavuelo, tpasajero listapasajero) {
    int nuevaSilla, sillaActual, intentos = 3, compraTiquete;
    char documento[30];

    cout << "Ingrese el numero de la silla actual :";
    cin >> sillaActual;

    while (*cvuelo != NULL)
    {
        if((*cvuelo)->silla == sillaActual){
           cout << "La silla fue encontrada" << endl;
           cout << "Necesitamos que nos confirmes tu numero de documento";
           while (intentos > 0)
           {
                cin >> documento;
                if(strcmp((*cvuelo)->asigancion->documento, documento) == 0){
                    cout << "Correcto !! Hemos confirmado tu identidad" << endl;
                    cout << "Por favor ingresa el nuevo numero de silla y validaremos su disponibilidad.";
                    cin >> nuevaSilla;
                    while (buscarSilla(*cvuelo, nuevaSilla) == 1)
                    {
                        cout << "La silla se encuentra asignada. Prueba con otra";
                        cin >> nuevaSilla;
                    }
                    
                    (*cvuelo)->silla = nuevaSilla;
                    cout << "Su nueva silla ha sido cambiada con exito"<< endl;
                    cout << "el nuevo numero de silla es : "<< nuevaSilla;

                    return 1;
                    
                }else{
                    cout << "Lo sentimos pero es incorrecto. ";
                    cout << "Vuelve a intentarlo (te quedan "<< intentos << " intentos )";
                    intentos--;
                }
           }
           cout << "No pudimos confirmar tu identidad. Por favor intentalo de nuevo mas tarde";
           return 0;
        }else{
            *cvuelo = (*cvuelo)->sig;
        }
    }
    
    cout << "No se ha encontrado el numero de silla ingresado. Desea comprar un tiquete ?" << endl;
    cout << "1. Si | 2 . No : ";
    cin >> compraTiquete;
    if(compraTiquete == 1){
        cout << "Perfecto!! Muchas gracias";
        comprarTiquete(&listavuelo, &listapasajero);
    }else{
        cout << "Ha sido un placer atenderte" << endl;
        cout << "Vuelve pronto...";
    }

    return 0;
}

int main()
{
    tpasajero lista_pasajero = NULL;
    tvuelo lista_vuelo = NULL;

    int opt=0;
    while (opt != 8){
        clearScreen();
        menu();
        cout << "Option: ";
        cin >> opt;
        cout << endl;
        switch (opt){
            case 1:
                comprarTiquete(&lista_vuelo, &lista_pasajero);
                break;
            case 2:
                modificarPasajero(&lista_pasajero);
                break;
            case 3:
                listarPasajeros(lista_pasajero);
                break;
            case 4:
                //buscarSilla(lista_vuelo, opt); // Prueba
                buscarPasajero(lista_pasajero);
                break;
            case 5:
                cambiarSilla(&lista_vuelo, lista_vuelo, lista_pasajero);
                break;
            case 6:
                imprimirPaseAbordar(lista_pasajero);
                break;
            case 7:
                cancelarTiquete(&lista_vuelo);
                break;
            case 8:
                cout << "/////////////////////////  BUEN VIAJE  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" << endl;
                break;
            // default :
            //   cout << "Opcion incorrecta!." << endl;
            //   break;
        }
        cout << "Press (enter) para continuar.";
        getchar();
    }
    return 0;
}
